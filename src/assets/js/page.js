console.log("Merry Christmas!");

// var seekProgressEl = document.querySelector("#seekAnim");
//
// seekProgressEl.addEventListener("input", function() {
//   timeline.seek(timeline.duration * (seekProgressEl.value / 100));
// });
//
// ["input", "change"].forEach(function(evt) {
//   seekProgressEl.addEventListener(evt, function() {
//     timeline.seek(timeline.duration * (seekProgressEl.value / 100));
//   });
// });

var timeline = anime.timeline({
  easing: "easeOutQuad",
  // update: function(anim) {
  //   seekProgressEl.value = anim.progress;
  // },
  autoplay: true
});

var imageDuration = 1500,
  textDuration = 800;

timeline
  .add({
    targets: "#textFrame0 img",
    opacity: 1,
    duration: 500,
    delay: 500
  })
  .add({
    targets: "#textFrame0 p",
    opacity: 1,
    duration: 500
  })
  .add({
    targets: "#textFrame1",
    opacity: 1,
    duration: 500,
    delay: 2000
  })
  .add({
    targets: "#textFrame1 p",
    opacity: 1,
    duration: 400
  })
  .add({
    targets: "#frame1",
    opacity: 1,
    duration: 500,
    delay: textDuration
  })
  .add({
    targets: "#frame1",
    scale: [3.5, 2.1],
    translateX: ["2.5%", "2.5%"],
    translateY: ["19%", "12%"],
    duration: 2000,
    offset: "-=500"
  })
  .add({
    targets: "#textFrame2",
    opacity: 1,
    duration: 500,
    complete: function(anim) {
      var prevImage = anim.animatables[0].target.previousElementSibling;
      prevImage.parentNode.removeChild(prevImage);
    }
  })
  .add({
    targets: "#textFrame2 p",
    opacity: 1,
    duration: 400
  })
  .add({
    targets: "#frame2",
    opacity: 1,
    duration: 500,
    delay: textDuration + 1000
  })
  .add({
    targets: "#frame2",
    scale: [6, 1.1],
    translateX: ["2%", "2%"],
    translateY: ["2%", "2%"],
    duration: 2200,
    offset: "-=500"
  })
  .add({
    targets: "#textFrame4",
    opacity: 1,
    duration: 500,
    delay: textDuration,
    complete: function(anim) {
      var prevImage = anim.animatables[0].target.previousElementSibling;
      prevImage.parentNode.removeChild(prevImage);
    }
  })
  .add({
    targets: "#textFrame4 p",
    opacity: 1,
    duration: 400
  })
  .add({
    targets: "#frame4",
    opacity: 1,
    duration: 500,
    delay: textDuration
  })
  .add({
    targets: "#textFrame5",
    opacity: 1,
    duration: 500,
    delay: 1000,
    complete: function(anim) {
      var prevImage = anim.animatables[0].target.previousElementSibling;
      prevImage.parentNode.removeChild(prevImage);
    }
  })
  .add({
    targets: "#textFrame5 p",
    opacity: 1,
    duration: 400
  })
  // .add({
  //   targets: "#frame5",
  //   opacity: 1,
  //   duration: 500,
  //   delay: textDuration
  // })
  // .add({
  //   targets: "#frame5",
  //   scale: [8, 1.1],
  //   // translateX: ["8%", 0],
  //   // translateY: ["7%", 0],
  //   duration: imageDuration + 1000,
  //   offset: "-=500"
  // })

  .add({
    targets: "#frame6",
    opacity: 1,
    duration: 500,
    delay: 100
  })
  .add({
    targets: "#frame6",
    scale: [10, 1.1],
    translateX: ["8%", 0],
    translateY: ["3%", 0],
    duration: imageDuration + 2000,
    offset: "-=500"
  })
  .add({
    targets: "#textFrame6",
    opacity: 1,
    duration: 500,
    complete: function(anim) {
      var prevImage = anim.animatables[0].target.previousElementSibling;
      prevImage.parentNode.removeChild(prevImage);
    }
  })
  .add({
    targets: "#textFrame6 p",
    opacity: 1,
    duration: 400
  })
  .add({
    targets: "#frame7",
    opacity: 1,
    duration: 500,
    delay: 1500
  })
  .add({
    targets: "#textFrame8",
    opacity: 1,
    duration: 500,
    delay: textDuration + 700,
    complete: function(anim) {
      var prevImage = anim.animatables[0].target.previousElementSibling;
      prevImage.parentNode.removeChild(prevImage);
    }
  })
  .add({
    targets: "#textFrame8 p",
    opacity: 1,
    duration: 400
  })
  .add({
    targets: "#frame10",
    opacity: 1,
    duration: 500,
    delay: textDuration
  })
  .add({
    targets: "#frame10",
    scale: [6, 1.1],
    duration: imageDuration + 3300,
    offset: "-=500"
  })
  .add({
    targets: "#textFrame11",
    opacity: 1,
    duration: 1000,
    delay: 1000
  })
  .add({
    targets: "#textFrame11 p.first",
    opacity: 1,
    duration: 400
  })
  .add({
    targets: "#textFrame11 p.logo",
    opacity: 1,
    duration: 400,
    delay: 1000
  });
